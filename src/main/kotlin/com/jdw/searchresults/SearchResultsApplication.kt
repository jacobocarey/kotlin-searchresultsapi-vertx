package com.jdw.searchresults

import io.vertx.core.Vertx
import io.vertx.core.DeploymentOptions

fun main() {
    val vertx = Vertx.vertx()
    vertx.deployVerticle("com.jdw.searchresults.verticles.ProductVerticle", DeploymentOptions().setWorker(true))
}