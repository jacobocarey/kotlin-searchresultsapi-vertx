package com.jdw.searchresults.verticles

import com.jdw.searchresults.service.ProductService
import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext

class ProductVerticle : AbstractVerticle() {

    private val productService = ProductService()

    override fun start(startFuture: Future<Void>) {
        val router = createRouter()

        vertx.createHttpServer()
            .requestHandler(router)
            .listen(config().getInteger("http.port", 8080)) { result ->
                if (result.succeeded()) {
                    println("Listening on port 8080.")
                    startFuture.complete()
                } else {
                    startFuture.fail(result.cause())
                }
            }
    }

    private fun createRouter() = Router.router(vertx).apply {
        get("/").handler(handlerRoot)
        get("/products").handler(handlerProducts)
        get("/products/:productId").handler(handlerProduct)
    }

    private val handlerRoot = Handler<RoutingContext> { req -> req.response().end("Search Result API") }

    private val handlerProducts = Handler<RoutingContext> { req ->
        req.response().endWithJson(productService.getAllProducts())
    }

    private val handlerProduct = Handler<RoutingContext> { req ->
        req.response().endWithJson(productService.getProduct(req.request().getParam("productId").toInt()))
    }

    fun HttpServerResponse.endWithJson(obj: Any) {
        this.putHeader("Content-Type", "application/json; charset=utf-8").end(Json.encodePrettily(obj))
    }
}
