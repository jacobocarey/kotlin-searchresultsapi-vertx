package com.jdw.searchresults.model

data class Product(val productId: Int, val description: String,  val imageUrl: String, val inSale: Boolean, val price: Double)