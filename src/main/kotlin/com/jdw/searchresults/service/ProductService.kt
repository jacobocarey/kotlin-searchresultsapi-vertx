package com.jdw.searchresults.service

import com.jdw.searchresults.ProductDataStub
import com.jdw.searchresults.model.Product

class ProductService {

    fun getAllProducts() : MutableList<Product> {
        var products: MutableList<Product> = mutableListOf()
        enumValues<ProductDataStub>().forEach { products.add(it.product) }
        return products
    }

    fun getProduct(id: Int): Product {
        println("Petting product with ID: F" + id)
        return enumValues<ProductDataStub>().filter { product -> product.id == id }.first().product
    }
}